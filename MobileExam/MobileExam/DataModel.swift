//
//  DataModel.swift
//  MobileExam
//
//  Created by PandaMaru on 11/21/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import Foundation
import SwiftyJSON

class DataModel {
    var url: String?
    var link: String?
    var text: String?
    
    init() {
    }
    
    init(json: JSON) {
        self.url = json["url"].stringValue
        self.link = json["link"].stringValue
        self.text = json["text"].stringValue
    }
    
    init(prototype: DataModel) {
        self.copy(prototype)
    }
    
    func copy(prototype: DataModel) {
        self.url = prototype.url
        self.link = prototype.link
        self.text = prototype.text
    }
}
