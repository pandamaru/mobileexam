//
//  WebViewController.swift
//  MobileExam
//
//  Created by PandaMaru on 11/21/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet var webView: UIWebView!
    @IBOutlet var loadIndicator: UIActivityIndicatorView!
    
    var webURL: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.delegate = self

        let requestObj = NSURLRequest(URL: NSURL(string: self.webURL)!);
        self.webView.loadRequest(requestObj);
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        self.loadIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.loadIndicator.stopAnimating()
    }
}
