//
//  APISingleton.swift
//  MobileExam
//
//  Created by PandaMaru on 11/21/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SwiftyJSON

class APISingleton {
    static let instance = APISingleton()
    
    private let baseURL = "http://project.enegist.com/examination"
    
    private init() {
    }
    
    func sealPromiseJSON(method: Alamofire.Method, APIName: String, parameters: [String : AnyObject]?) -> Promise<JSON> {
        return Promise { fulfill, reject in
            Alamofire.request(method, baseURL + "/" + APIName, parameters: parameters).responseJSON { response in
                switch response.result {
                case .Success(let data):
                    let json = JSON(data)
                    fulfill(json)
                case .Failure(let error):
                    reject(error)
                }
            }
        }
    }
    
    func getTestJSON() -> Promise<JSON> {
        return sealPromiseJSON(.GET, APIName: "mobile.json.php", parameters: nil)
    }
}