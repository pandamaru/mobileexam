//
//  ContentViewController.swift
//  MobileExam
//
//  Created by PandaMaru on 11/21/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import UIKit
import Kingfisher

class ContentViewController: UIViewController {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var captionLabel: UILabel!
    
    var navViewController: UIViewController!
    var dataModel: DataModel!
    var pageIndex: Int!
    var tapImage: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tapImage = UITapGestureRecognizer(target: self, action: Selector("handleTap:"))
        imageView.addGestureRecognizer(tapImage)
        
        self.captionLabel.text = self.dataModel.text
        self.imageView.kf_setImageWithURL(NSURL(string: self.dataModel.url!)!, placeholderImage: nil)
        //self.imageView.kf_setImageWithURL(NSURL(string: self.dataModel.url!)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleTap(recognizer: UITapGestureRecognizer) {
        self.navViewController.performSegueWithIdentifier("WebViewController", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
