//
//  MainViewController.swift
//  MobileExam
//
//  Created by PandaMaru on 11/21/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import UIKit
import PromiseKit
import SwiftyJSON

class MainViewController: UIViewController, UIPageViewControllerDataSource {
    @IBOutlet var loadIndicator: UIActivityIndicatorView!
    
    var pageViewController: UIPageViewController!
    var dataModelList: NSMutableArray!
    var timer: NSTimer!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataModelList = NSMutableArray()
        
        self.loadIndicator.startAnimating()
        APISingleton.instance.getTestJSON().then { json -> Void in
            for (_, subJson):(String, JSON) in json {
                self.dataModelList.addObject(DataModel(json: subJson))
            }
        }.always {
            self.loadIndicator.stopAnimating()
            
            self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
            self.pageViewController.dataSource = self
            
            let firstContentViewController = self.contentViewControllerAtIndex(0)
            var contentViewControllers = [ContentViewController]()
            contentViewControllers.append(firstContentViewController)
            
            self.pageViewController.setViewControllers(contentViewControllers, direction: .Forward, animated: true, completion: nil)
            self.view.addSubview(self.pageViewController.view)
            
            self.setTimer()
        }.error { error in
            print(error)
            UIAlertView(title: "Error", message: "Something bad happens T-T", delegate: nil, cancelButtonTitle: "Dismiss").show()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if (self.dataModelList.count > 0) {
            timer.invalidate()
            self.setTimer()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        timer.invalidate()
        
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getNextIndex(currentIndex: Int) -> Int {
        var index = currentIndex
        
        if (index < self.dataModelList.count - 1) {
            index++
        } else {
            index = 0
        }
        
        return index
    }
    
    func getPreviousIndex(currentIndex: Int) -> Int {
        var index = currentIndex
        
        if (index > 0) {
            index--
        } else {
            index = self.dataModelList.count - 1
        }
        
        return index
    }
    
    func setTimer() {
        self.timer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: Selector("autoChangePage"), userInfo: nil, repeats: true)
    }
    
    func contentViewControllerAtIndex(index: Int) -> ContentViewController {
        if (self.dataModelList.count == 0) {
            //Empty data
            return ContentViewController()
        }
        
        if (index < 0 || index >= self.dataModelList.count) {
            //index out of bound
            return ContentViewController()
        }
        
        let contentViewController: ContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController
        contentViewController.navViewController = self
        contentViewController.dataModel = dataModelList[index] as! DataModel
        contentViewController.pageIndex = index
        
        return contentViewController
    }
    
    func autoChangePage() {
        let viewControllers = self.pageViewController.viewControllers![0] as! ContentViewController
        
        var index: Int = viewControllers.pageIndex
        index = self.getNextIndex(index)
        
        let nextContentViewController = self.contentViewControllerAtIndex(index)
        var contentViewControllers = [ContentViewController]()
        contentViewControllers.append(nextContentViewController)
        
        self.pageViewController.setViewControllers(contentViewControllers, direction: .Forward, animated: true, completion: nil)

    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "WebViewController") {
            let webViewController:WebViewController = segue.destinationViewController as! WebViewController
            let contentViewController:ContentViewController = sender as! ContentViewController
            let dataModel = self.dataModelList[contentViewController.pageIndex] as! DataModel
            webViewController.webURL = dataModel.link
            webViewController.navigationItem.title = dataModel.text
        }
    }
    
    // MARK: - Page View Controller Data Source
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let contentViewController = viewController as! ContentViewController
        var index = contentViewController.pageIndex as Int
        
        index = self.getNextIndex(index)
        
        timer.invalidate()
        self.setTimer()
        
        return self.contentViewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let contentViewController = viewController as! ContentViewController
        var index = contentViewController.pageIndex as Int
        
        index = self.getPreviousIndex(index)
        
        timer.invalidate()
        self.setTimer()

        return self.contentViewControllerAtIndex(index)
    }
}
